package calculator.operator;

import org.junit.Test;

import static org.junit.Assert.*;

public class MultiplicationOperatorTest {


    @Test
    public void shouldMatchForMultiplicationToken() {
        //WHEN
        String token = "*";
        MultiplicationOperator multiplicationOperator = new MultiplicationOperator();
        //THEN
        assertTrue(multiplicationOperator.isMatching(token));
    }

    @Test
    public void shouldNOTMatchForWrongToken() {
        //WHEN
        String token = "+";
        MultiplicationOperator multiplicationOperator = new MultiplicationOperator();
        //THEN
        assertFalse(multiplicationOperator.isMatching(token));
    }

    @Test
    public void shouldCorrectlyMultiplyTwoNumbers() {
        //WHEN
        Long firstNum = 3L;
        Long secondNum = 500L;
        MultiplicationOperator multiplicationOperator = new MultiplicationOperator();
        //THEN
        Long expected = 1500L;
        assertEquals(expected, multiplicationOperator.executeOperator(firstNum, secondNum));
    }
}