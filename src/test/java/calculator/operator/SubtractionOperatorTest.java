package calculator.operator;

import org.junit.Test;

import static org.junit.Assert.*;

public class SubtractionOperatorTest {


    @Test
    public void shouldMatchForSubtractionToken() {
        //WHEN
        String token = "-";
        SubtractionOperator subtractionOperator = new SubtractionOperator();
        //THEN
        assertTrue(subtractionOperator.isMatching(token));
    }

    @Test
    public void shouldNOTMatchForWrongToken() {
        //WHEN
        String token = "+";
        SubtractionOperator subtractionOperator = new SubtractionOperator();
        //THEN
        assertFalse(subtractionOperator.isMatching(token));
    }

    @Test
    public void shouldCorrectlySubtractTwoNumbers() {
        //WHEN
        Long firstNum = 550L;
        Long secondNum = 500L;
        SubtractionOperator subtractionOperator = new SubtractionOperator();
        //THEN
        Long expected = -50L;
        assertEquals(expected, subtractionOperator.executeOperator(firstNum, secondNum));
    }
}