package calculator.operator;

import org.junit.Test;

import static org.junit.Assert.*;

public class AdditionOperatorTest {

    @Test
    public void shouldMatchForAdditionToken() {
        //WHEN
        String token = "+";
        AdditionOperator additionOperator = new AdditionOperator();
        //THEN
        assertTrue(additionOperator.isMatching(token));
    }

    @Test
    public void shouldNOTMatchForWrongToken() {
        //WHEN
        String token = "-";
        AdditionOperator additionOperator = new AdditionOperator();
        //THEN
        assertFalse(additionOperator.isMatching(token));
    }

    @Test
    public void shouldCorrectlyAddTwoNumbers() {
        //WHEN
        Long firstNum = 20L;
        Long secondNum = 500L;
        AdditionOperator additionOperator = new AdditionOperator();
        //THEN
        Long expected = 520L;
        assertEquals(expected, additionOperator.executeOperator(firstNum, secondNum));
    }
}