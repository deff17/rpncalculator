package calculator;

import calculator.validation.MalformedEquationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class RpnCalculatorTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldCountCorrectResult() throws Exception {
        //WHEN
        String equation = "5 1 2 + 4 * + 3 -";
        RpnCalculator rpnCalculator = new RpnCalculator();
        Long expected = 14L;
        //THEN
        Long actual = rpnCalculator.calculate(equation);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldThrowMalformedEquationExceptionForNotSupportedOperation() throws Exception {
        //WHEN
        expectedException.expect(MalformedEquationException.class);
        expectedException.expectMessage("/ is not supported operator");
        String equation = "20 5 /";
        RpnCalculator rpnCalculator = new RpnCalculator();
        rpnCalculator.calculate(equation);
    }

    @Test
    public void shouldThrowMalformedEquationExceptionForNotRPNEquation() throws Exception {
        //WHEN
        expectedException.expect(MalformedEquationException.class);
        expectedException.expectMessage("equations is not in RPN format");
        String equation = "20 + 5";
        RpnCalculator rpnCalculator = new RpnCalculator();
        //THEN
        rpnCalculator.calculate(equation);
    }

    @Test
    public void shouldThrowMalformedEquationExceptionForEmptyString() throws Exception {
        //WHEN
        expectedException.expect(MalformedEquationException.class);
        expectedException.expectMessage("empty equation was entered");
        String equation = " ";
        RpnCalculator rpnCalculator = new RpnCalculator();
        //THEN
        rpnCalculator.calculate(equation);
    }


    @Test
    public void shouldCalculateNegativeNumbers() throws Exception {
        //WHEN
        String equation = "-20 -30 *";
        RpnCalculator rpnCalculator = new RpnCalculator();
        Long expected = 600L;
        //THEN
        Long actual = rpnCalculator.calculate(equation);
        assertEquals(expected, actual);
    }

    @Test
    public void shoulThrowMalformedEquationExceptionForMoreEmptyTokens() throws Exception {
        //WHEN
        expectedException.expect(MalformedEquationException.class);
        expectedException.expectMessage("\" \" is not supported operator");
        String equation = "20   5   +";
        RpnCalculator rpnCalculator = new RpnCalculator();
        rpnCalculator.calculate(equation);
    }

}