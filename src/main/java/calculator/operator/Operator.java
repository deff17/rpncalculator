package calculator.operator;

public interface Operator {

    boolean isMatching(String operator);
    Long executeOperator(Long firstOperand, Long secondOperand);
}
