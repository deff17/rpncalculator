package calculator.operator;

public class SubtractionOperator implements Operator {

    public boolean isMatching(String operator) {
        return operator.equals("-");
    }

    public Long executeOperator(Long firstOperand, Long secondOperand) {
        return secondOperand - firstOperand;
    }
}
