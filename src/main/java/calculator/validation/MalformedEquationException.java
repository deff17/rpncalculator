package calculator.validation;

public class MalformedEquationException extends Exception {

    public MalformedEquationException(String errorMessage) {
        super(errorMessage);
    }
}
