package calculator;

import calculator.operator.AdditionOperator;
import calculator.operator.MultiplicationOperator;
import calculator.operator.Operator;
import calculator.operator.SubtractionOperator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OperatorFactory {

    private static final AdditionOperator additionOperator = new AdditionOperator();
    private static final SubtractionOperator subtractionOperator = new SubtractionOperator();
    private static final MultiplicationOperator multiplicationOperator = new MultiplicationOperator();
    private List<Operator> operators = new ArrayList<>();

    public OperatorFactory() {
        this.operators.add(additionOperator);
        this.operators.add(subtractionOperator);
        this.operators.add(multiplicationOperator);
    }

    public Operator getOperator(String operator) {
        List<Operator> singleOperatorList = this.operators.stream()
                .filter(o -> o.isMatching(operator))
                .collect(Collectors.toList());

        return singleOperatorList.size() > 0 ? singleOperatorList.get(0) : null;
    }

}
