package calculator;

import calculator.operator.Operator;
import calculator.validation.MalformedEquationException;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RpnCalculator {

    private static final OperatorFactory operatorFactory = new OperatorFactory();

    public Long calculate(String equation) throws Exception {
        LinkedList<Long> stack = new LinkedList<>();
        String[] tokens = equation.split(" ");

        if (equation.trim().isEmpty()) {
            throw new MalformedEquationException("empty equation was entered");
        }

        for (String token : tokens) {
            if (isOperator(token) && stack.size() > 1) {
                Long firstDigit = stack.removeLast();
                Long secondDigit = stack.removeLast();
                Operator operator = operatorFactory.getOperator(token);
                stack.add(operator.executeOperator(firstDigit, secondDigit));
            } else {
                if (this.isTokenANumber(token)) {
                    stack.add(Long.valueOf(token));
                } else {
                    throw new MalformedEquationException("equations is not in RPN format");
                }
            }
        }

        if (stack.size() > 1) {
            throw new MalformedEquationException("equations is not in RPN format");
        }

        return stack.removeLast();
    }

    private boolean isOperator(String token) throws MalformedEquationException {
        if (!this.isTokenANumber(token)) {
            if (operatorFactory.getOperator(token) != null) {
                return true;
            } else {
                token = token.trim().isEmpty() ? "\" \"" : token;
                throw new MalformedEquationException(token + " is not supported operator.");
            }
        }
        return false;
    }

    private boolean isTokenANumber(String token) {
        Pattern isANumberPattern = Pattern.compile("-?[0-9]+");
        Matcher isANumberMatcher = isANumberPattern.matcher(token);
        return isANumberMatcher.matches();
    }
}
