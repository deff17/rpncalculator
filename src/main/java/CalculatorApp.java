import calculator.RpnCalculator;
import calculator.validation.MalformedEquationException;

import java.util.Scanner;

public class CalculatorApp {

    public static void main(String[] args) {

        System.out.println("Enter equation in RPN format \n" +
                "Example: 20 5 + \n" +
                "To exit enter q character");

        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
            String equation = scanner.nextLine();
            if (equation.equals("q")) {
                System.out.println("You have exited application");
                break;
            }

            RpnCalculator calculator = new RpnCalculator();
            try {
                System.out.println(calculator.calculate(equation.trim()));
            } catch (MalformedEquationException exception) {
                System.out.println("Invalid equation: " + exception.getMessage());
            } catch (Exception exception) {
                System.out.println("Invalid equation was entered " + exception);
            }
        }
    }
}
